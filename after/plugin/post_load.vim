setlocal tabstop=2
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal noexpandtab
setlocal autoindent

nnoremap <C-j> <c-w>j
nnoremap <C-k> <c-w>k
nnoremap <C-h> <c-w>h
nnoremap <C-l> <c-w>l
