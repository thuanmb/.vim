"defaults write NSGlobalDomain KeyRepeat -int 1
"defaults write NSGlobalDomain InitialKeyRepeat -int 10

syntax on

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

" theme config
set t_Co=256
set cursorline

colorscheme onedark
let g:airline_theme='onedark'
let g:lightline = { 'colorscheme': 'onedark' }

" show more log details when using vim
" set verbose=2

" colorscheme onehalflight
" let g:airline_theme='onehalfdark'
" let g:lightline = { 'colorscheme': 'onehalfdark' }


" vim config
set lazyredraw
set number
set nowrap
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4
set smartindent
set autoindent
set hidden
set history=100
set hlsearch
set showmatch
set autoread
set noswapfile
set nocompatible
set incsearch
set formatoptions+=j
set scrolloff=1
set laststatus=2
set mouse=a
set clipboard^=unnamed,unnamedplus
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/node_modules
set noeb vb t_vb=
filetype on
filetype indent on
filetype plugin on
filetype plugin indent on
set list
set listchars=tab:‣\ ,space:·,precedes:…,extends:…,eol:¬

" fix redrawtime exceeded syntax highlighting disabled error
set re=2

let mapleader=" "
nnoremap <silent> <tab> %
vnoremap <silent> <tab> %
nnoremap S "_diwP
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>vs :source ~/.vim/vimrc<CR>
nnoremap <leader><leader> :e#<CR>
nnoremap H :tabprevious<CR>
nnoremap L :tabnext<CR>
nnoremap <leader>fp :let @*=expand("%:p")<CR>
nnoremap <leader>fn :let @*=expand("%")<CR>
nnoremap <leader>pp "0p
nnoremap <leader>po "0P
nnoremap <leader>b :TernDef<CR>
nnoremap <leader>r :TernRename<CR>
nnoremap <CR> i<CR><Esc>O
nnoremap <C-o> o<Esc>
nnoremap <C-p> O<Esc>
" nnoremap <leader>dc v/,<CR>hd
" nnoremap <leader>db v/}<CR>d
" nnoremap <leader>dp v/)<CR>d
nnoremap <leader>i} f}hi
nnoremap <leader>i] f]i
nnoremap <leader>i) f)i
nnoremap <leader>uf zf'a
nnoremap <leader>nl f,a<CR><Esc>

" java
nnoremap <leader>oi :CocCommand editor.action.organizeImport<CR>

" ==========================================
" utils
" ==========================================
" <div /> ==> <div></div>
nnoremap <leader>de yiwt/dt>a</<C-R>0><esc>T>i

" remove whitespaces on saving
autocmd BufWritePre * :%s/\s\+$//e

" pluginconfig
" ==========================================
" nerdcommenter
" ==========================================
let g:NERDSpaceDelims = 1
let g:NERDCustomDelimiters = { 'javascript': { 'left': '//', 'right': '', 'leftAlt': '{/*', 'rightAlt': '*/}' } }

" pluginconfig
" ==========================================
" easymotion
" ==========================================
map <leader>m <Plug>(easymotion-prefix)
map <leader>ml <Plug>(easymotion-bd-jk)
nmap <leader>ml <Plug>(easymotion-overwin-line)

" pluginconfig
" ==========================================
" qfenter
" ==========================================
let g:qfenter_keymap = {}
let g:qfenter_keymap.open = ['o', '<CR>']
let g:qfenter_keymap.vopen = ['v']
let g:qfenter_keymap.hopen = ['h']
let g:qfenter_keymap.topen = ['t']

" pluginconfig
" ==========================================
" nerdtree
" ==========================================
nnoremap <leader>tt :NERDTreeToggle<CR>
nnoremap <leader>tr :NERDTreeFind<CR>

" pluginconfig
" ==========================================
" fzf.vim
" ==========================================
let g:fzf_history_dir = '~/.vim/fzf-history'
let g:fzf_files_options = '--bind alt-a:select-all,alt-d:deselect-all'
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-h': 'split',
  \ 'ctrl-v': 'vsplit' }
command! -bang -nargs=* Ag
      \ call fzf#vim#ag(<q-args>,
      \                 <bang>0 ? fzf#vim#with_preview('up:60%')
      \                         : fzf#vim#with_preview('right:50%:hidden', '?'),
      \                 <bang>0)
command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
      \   <bang>0 ? fzf#vim#with_preview('up:60%')
      \           : fzf#vim#with_preview('right:50%:hidden', '?'),
      \   <bang>0)
nnoremap <Leader>fa :Files<CR>
nnoremap <Leader>fg :GFiles<CR>
nnoremap <Leader>fb :Buffers<CR>
nnoremap <Leader>fh :History<CR>
nnoremap <Leader>fl :BLines<CR>

" pluginconfig
" ==========================================
" text search ack.vim + ripgrep
" ==========================================
let g:ack_use_cword_for_empty_search = 1
let g:ackhighlight = 1
let g:ackprg = 'rg --vimgrep --no-heading --fixed-strings'
let g:ack_apply_qmappings = 0
let g:ack_apply_lmappings = 0
nnoremap <Leader>sb :Ack! ''<Left>
nnoremap <Leader>sq :Ack!<CR>
nnoremap <Leader>sl :LAck!<CR>
vnoremap <Leader>sq y:Ack! '<C-r>"'<CR>
vnoremap <Leader>sl y:LAck! '<C-r>"'<CR>
nnoremap <leader>sc :nohlsearch<CR>


" pluginconfig
" ==========================================
" git-fugitive
" ==========================================
nnoremap <leader>gs :Git<CR>
nnoremap <leader>gc :Git commit<CR>
nnoremap <leader>gw :Gwrite<CR>
nnoremap <leader>gd :Git diff<CR>
nnoremap <leader>gp :Git! push<CR>
nnoremap <leader>gb :Git blame<CR>


" pluginconfig
" ==========================================
" vim-go
" ==========================================
nnoremap <leader>gr :GoRun<CR>
nnoremap <leader>gt :GoTest<CR>
nnoremap <leader>gf :GoTestFunc<CR>
nnoremap <leader>gl :GoLint<CR>

" pluginconfig
" ==========================================
" NERDTree
" ==========================================
let g:NERDTreeIgnore=['node_modules', '\.git$', '\~$']

" pluginconfig
" ==========================================
" syntastic
" ==========================================
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 1
" javascript lint
let g:syntastic_mode_map = { 'mode': 'passive' }
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exe = '$(npm bin)/eslint'

" eslint require following libs to work and .eslintrc files
" npm install --save-dev eslint eslint-config-airbnb babel-eslint eslint-plugin-react eslint-plugin-import eslint-plugin-jsx-a11y
" http://remarkablemark.org/blog/2016/09/28/vim-syntastic-eslint/

" when running at every change you may want to disable quickfix
" augroup autoformat_settings
  " autocmd FileType java AutoFormatBuffer google-java-format
" augroup END
let g:prettier#quickfix_enabled = 0
let g:prettier#autoformat = 0
let g:prettier#config#print_width = 120
autocmd BufWritePre *.js,*.jsx,*.es6,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.vue,*.md,*.yaml PrettierAsync
" autocmd BufWritePre *.js,*.jsx,*.es6,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.graphql,*.vue PrettierAsync

" ==========================================
" Python
" ==========================================
let python_highlight_all = 1
let g:SimpylFold_docstring_preview = 1
autocmd FileType python setlocal foldenable foldlevel=20

autocmd BufWritePost *.py call flake8#Flake8()
" Hide the flake8 quick fix
"let g:flake8_show_quickfix=0
let g:flake8_show_in_gutter=1
let g:flake8_show_in_file=0
"
" pluginconfig
" ==========================================
" Java
" ==========================================
" autocmd FileType java setlocal omnifunc=javacomplete#Complete

autocmd FileType java let g:coc_root_pattern = ['backend']

" g:JavaComplete_Home

nmap <F4> <Plug>(JavaComplete-Imports-AddSmart)
nmap <F5> <Plug>(JavaComplete-Imports-Add)

" pluginconfig
" ==========================================
" startify
" ==========================================
let g:startify_skiplist = [
            \ '.git/index',
            \ '.git/config',
            \ ]
let g:startify_bookmarks = [
            \ { 'u': '/Volumes/Development/source_code/nio-api' },
            \ { 'b': '~/.bash_profile' },
            \ { 'c': '/Volumes/Development/source_code/nio-conf' },
            \ { 'd': '/Volumes/Development/source_code/nio-base' },
            \ { 'e': '/Volumes/Development/source_code/nio-scrappers' },
            \ { 'f': '/Volumes/Development/source_code/nio-golang' },
            \ { 'j': '/Volumes/Development/source_code/nio-japi' },
            \ { 'l': '/Volumes/Development/source_code/nio-python3-libs' },
            \ { 'h': '/Volumes/Development/source_code/nio-build-deps' },
            \ { 'i': '/Volumes/Development/source_code/learning-eth/scaffold-eth' },
            \ { 'k': '/Volumes/Development/source_code/nio-api-auth' },
            \ { 'g': '/Volumes/Development/source_code/nio-python-libs' },
            \ { 'n': '/Volumes/Development/source_code/nio-analytics' },
            \ { 'm': '/Volumes/Development/source_code/nio-maps-scopes' },
            \ { 'o': '/Volumes/Development/source_code/ariadni' },
            \ { 'p': '/Volumes/Development/source_code/nio-ui-packages' },
            \ { 'r': '/Volumes/Development/source_code/rbuild.conf' },
            \ { 's': '/Volumes/Development/source_code/nio-system' },
            \ { 't': '/Volumes/Development/source_code/nio-conf-templates' },
            \ { 'a': '/Volumes/Development/source_code/nio-api3' },
            \ { 'v': '~/.vim/vimrc' },
            \ { 'w': '/Volumes/Development/source_code/nio-web' },
            \ ]
let g:startify_list_order = [
            \ ['   Bookmarks'],
            \ 'bookmarks',
            \ ['   MRU'],
            \ 'files',
            \ ['   Sessions'],
            \ 'sessions',
            \ ]
let g:startify_custom_header = [
            \'',
            \'',
            \'',
            \'',
            \'',
            \'',
            \'       (^.^)       ',
            \'',
            \' No pain. No gain! ',
            \'',
            \'',
            \'',
            \]
let g:startify_session_delete_buffers = 1
let g:startify_session_persistence = 1
let g:startify_disable_at_vimenter= 0
let g:startify_session_before_save = ['silent! NERDTreeClose']

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
" autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd BufNewFile,BufRead *.js,*.jsx setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd BufNewFile,BufRead *.js,*.jsx,*.css setlocal tabstop=2 shiftwidth=2 softtabstop=2 noexpandtab autoindent
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.php,*.jsx"
let g:tern#is_show_argument_hints_enabled = 0

au BufNewFile,BufRead *.i set filetype=swig
au BufNewFile,BufRead *.swig set filetype=swig

" vim-dispatch
autocmd FileType * let b:dispatch = '/Volumes/Development/source_code/rbuild.sh '.getcwd().' -sAab'
nnoremap <leader>dp :Dispatch!<CR>

" auto PEP
let g:autopep8_disable_show_diff=1
let g:autopep8_on_save = 1
let g:autopep8_max_line_length=160
let g:autopep8_ignore="E501,E402"
" let g:autopep8_indent_size=2

" plugin config
" Shougo/deoplete.nvim
let g:deoplete#enable_at_startup = 1

" pluginconfig
" ==========================================
" undotree
" ==========================================
nnoremap <leader>u :UndotreeToggle<cr>

" pluginconfig
" ==========================================
" vim-json
" ==========================================
let g:vim_json_syntax_conceal = 0

" Vundle Pluggins
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'fatih/vim-go'
Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'tpope/vim-dispatch'
Plugin 'mileszs/ack.vim'
Plugin 'rbgrouleff/bclose.vim'
Plugin 'corntrace/bufexplorer'
Plugin 'preservim/nerdtree'
Plugin 'preservim/nerdcommenter'
Plugin 'godlygeek/csapprox'
Plugin 'tpope/vim-fugitive'
Plugin 'gregsexton/gitv'
Plugin 'airblade/vim-gitgutter'
" cd ~/.vim/bundle/coc.nvim
" yarn install --frozen-lockfile
Plugin 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}
Plugin 'jiangmiao/auto-pairs'
Plugin 'sheerun/vim-polyglot'
Plugin 'itchyny/lightline.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'easymotion/vim-easymotion'
Plugin 'yssl/QFEnter'
Plugin 'junegunn/fzf', { 'do': './install --bin' }
Plugin 'junegunn/fzf.vim'
Plugin 'prettier/vim-prettier', { 'do': 'yarn install' }
" Add maktaba and codefmt to the runtimepath.
" (The latter must be installed before it can be used.)
Plugin 'google/vim-maktaba'
Plugin 'google/vim-codefmt'
" Also add Glaive, which is used to configure codefmt's maktaba flags. See
" `:help :Glaive` for usage.
Plugin 'google/vim-glaive'
Plugin 'mhinz/vim-startify'
Plugin 'tell-k/vim-autopep8'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-endwise'
Plugin 'tpope/vim-repeat'
Plugin 'mbbill/undotree'
Plugin 'elzr/vim-json'

" Plugin 'artur-shaik/vim-javacomplete2'


" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on

" the glaive#Install() should go after the "call vundle#end()"
" call glaive#Install()
" Glaive codefmt google_java_executable="java -jar /Users/bui/.vim/jars/google-java-format-1.8-all-deps.jar --aosp --fix-imports-only"
